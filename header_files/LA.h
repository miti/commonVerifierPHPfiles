//
// Created by miti on 2019-12-21.
//
#ifndef VERIFIER_LA_H
#define VERIFIER_LA_H
#include "ProtobufMessageRW.h"
#include "ProtobufLAMessages.pb.h"

struct _sgx_dh_session_t;
typedef _sgx_dh_session_t sgx_dh_session_t;

class LA {
    sgx_dh_session_t* sgx_dh_session;
    // dh_session_t global_session_info;
    ProtobufMessageRW protobufReaderWriter;
    uint8_t key[16];
    uint32_t process_protobuf_dh_msg3(protobuf_sgx_dh_msg3_t& protobuf_msg3);
    uint32_t process_protobuf_dh_msg1_generate_protobuf_dh_msg2(protobuf_sgx_dh_msg1_t& protobuf_msg1, protobuf_sgx_dh_msg2_t& protobuf_msg2);
public:
    uint32_t conduct_la(int fd);
    void get_la_symmetric_key(uint8_t* key);
};
#endif //LAINITIATOR_PROTOBUFINTERFACE_H
