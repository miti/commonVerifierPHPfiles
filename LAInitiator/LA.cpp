#include "sgx_eid.h"
#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#include <stdio.h>

#include "sgx_trts.h"
#include "sgx_utils.h"
#include "error_codes.h"
#include "sgx_ecp_types.h"
#include "sgx_thread.h"
#include "sgx_tcrypto.h"
#include "datatypes.h"
#include "Transforms.h"
#define MAX_SESSION_COUNT  16
#define SGX_CAST(type, item) ((type)(item))
#include <string.h>
#include "crypto.h"
#include "LA.h"
uint32_t LA::process_protobuf_dh_msg1_generate_protobuf_dh_msg2(protobuf_sgx_dh_msg1_t& protobuf_msg1, protobuf_sgx_dh_msg2_t& protobuf_msg2)
{
  sgx_dh_msg1_t dh_msg1;            //Diffie-Hellman Message 1
  sgx_dh_msg2_t dh_msg2;
  memset(&dh_msg1, 0, sizeof(sgx_dh_msg1_t));
  uint32_t ret_status;

  if(Transforms::decode_msg1_from_protobuf(protobuf_msg1, &dh_msg1)!=0)
    return 0x1;

  //Intialize the session as a session initiator
  sgx_dh_session = (sgx_dh_session_t*) malloc(sizeof(sgx_dh_session_t));
  ret_status = sgx_dh_init_session(SGX_DH_SESSION_INITIATOR, sgx_dh_session);
  if(ret_status != SGX_SUCCESS)
    return ret_status;

  //Process the message 1 obtained from desination enclave and generate message 2
  ret_status = sgx_dh_initiator_proc_msg1(&dh_msg1, &dh_msg2, sgx_dh_session);
  if(ret_status != SGX_SUCCESS)
    return ret_status;

  Transforms::encode_msg2_to_protobuf(protobuf_msg2, &dh_msg2);
  return 0;
}

uint32_t LA::process_protobuf_dh_msg3(protobuf_sgx_dh_msg3_t& protobuf_msg3) {

  uint32_t ret_status;
  sgx_dh_msg3_t dh_msg3;
  sgx_key_128bit_t dh_aek;        // Session Key
  sgx_dh_session_enclave_identity_t responder_identity;

  memset(&dh_aek,0, sizeof(sgx_key_128bit_t));

  if(Transforms::decode_msg3_from_protobuf(protobuf_msg3, &dh_msg3)!=0)
    return -1;

  //Process Message 3 obtained from the destination enclave
  ret_status = sgx_dh_initiator_proc_msg3(&dh_msg3, sgx_dh_session, &dh_aek, &responder_identity);
  if(SGX_SUCCESS != ret_status)
    return ret_status;

  //memcpy(global_session_info.active.AEK, &dh_aek, sizeof(sgx_key_128bit_t));
  memcpy(key, &dh_aek, sizeof(sgx_key_128bit_t));
  /*global_session_info.session_id = 1; // TODO: session_id;
  global_session_info.active.counter = 0;
  global_session_info.status = ACTIVE;
   */
  memset(&dh_aek,0, sizeof(sgx_key_128bit_t));

  return 0;
}

uint32_t LA::conduct_la(int decryptor_fd) {
    // declare msg1, msg2, msg3 protobuf objects
    protobuf_sgx_dh_msg1_t protobuf_msg1;
    protobuf_sgx_dh_msg2_t protobuf_msg2;
    protobuf_sgx_dh_msg3_t protobuf_msg3;
    uint32_t protobuf_sgx_ret;

    setbuf(stdout,NULL);

    protobufReaderWriter.set_fd(decryptor_fd);

    printf("Reading message 1\n"); fflush(stdout);
    if(protobufReaderWriter.read_msg(protobuf_msg1)!=0)
        return -1;

    printf("Generating message 2\n"); fflush(stdout);
    protobuf_sgx_ret = process_protobuf_dh_msg1_generate_protobuf_dh_msg2(protobuf_msg1, protobuf_msg2);
    if(protobuf_sgx_ret != 0)
    {
        printf("Error in process_protobuf_dh_msg1_generate_protobuf_dh_msg2: 0x%x", protobuf_sgx_ret); fflush(stdout); return protobuf_sgx_ret;
    }

    printf("Writing message 2\n"); fflush(stdout);
    if(protobufReaderWriter.write_msg(protobuf_msg2)!=0)
        return -1;

    printf("Reading message 3\n"); fflush(stdout);
    if(protobufReaderWriter.read_msg(protobuf_msg3)!=0)
        return -1;

    printf("Processing message 3\n"); fflush(stdout);
    protobuf_sgx_ret = process_protobuf_dh_msg3(protobuf_msg3);
    if(protobuf_sgx_ret != 0)
    {
        printf("Error in process_protobuf_dh_msg3: 0x%x", protobuf_sgx_ret); fflush(stdout); return protobuf_sgx_ret;
    }
    return 0;
}

void LA::get_la_symmetric_key(uint8_t* op_key)
{
    uint32_t counter;
    for(counter=0; counter<16; counter++)
        op_key[counter] = key[counter];
}
